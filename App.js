/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * 
 * 
 * 
import Settings from './src/screens/Settings';
import Settings from './src/screens/Home';
 */

 // App.js
 
import {  createStackNavigator,  createAppContainer,} from 'react-navigation';
import Settings from './src/screens/Settings.js'
import Home from './src/screens/Home.js'

const AppNavigator = createStackNavigator({

  Home: {
    screen: Home
  },
  Settings: {
    screen: Settings,
    navigationOptions: {
      header: null,
    }
  }
});

const App = createAppContainer(AppNavigator);

export default App;